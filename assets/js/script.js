//Selectors
const todoInput = document.querySelector(".todo-input");
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const filterOption = document.querySelector(".filter-todo");
//Event Listeners
todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteCheck);
filterOption.addEventListener("click", filterTodo);
document.addEventListener("DOMContentLoaded", getTodos);
//Functions

function addTodo(e) {
	e.preventDefault();
	// console.log("hello");
	const todoDiv = document.createElement("div");
	todoDiv.classList.add("todo");

	const newTodo = document.createElement("li");
	newTodo.innerText = todoInput.value;
	newTodo.classList.add("todo-item");

	todoDiv.appendChild(newTodo);
	//ADD todo to the local storage
	saveLocalTodos(todoInput.value);
	//Check Completed Butoon
	const completedButton = document.createElement("button");
	completedButton.innerHTML = '<i class="fas fa-check"></i>';
	completedButton.classList.add("complete-btn");
	todoDiv.appendChild(completedButton);
	//CHECK trash Button
	const trashButton = document.createElement("button");
	trashButton.innerHTML = '<i class="fas fa-trash"></i>';
	trashButton.classList.add("trash-btn");
	todoDiv.appendChild(trashButton);
	//APPEND TO LIST
	todoList.appendChild(todoDiv);

	//Clear todoInput Value
	todoInput.value = "";
}

function deleteCheck(e) {
	// console.log(e.target);
	const item = e.target;
	//DELETE TODO
	if(item.classList[0] === "trash-btn") {
		/* At the 1st look, its weird when i saw that theres no error when we redeclare the todo as a class. But looking
		at it more carefully,i figured it out that we declare the second todo in a function and in the if statement, not using it as a permanent class declaration for a class.  */
		const todo = item.parentElement;
		//Animation
		todo.classList.add("fall");
		removeLocalTodos(todo);
		todo.addEventListener("transitionend", function() {
			todo.remove();
		});
	}
	//CHECK MARK
	if(item.classList[0] === "complete-btn") {
		const todo = item.parentElement;
		todo.classList.toggle("completed");
	}
}

function filterTodo(e) {
	const todos = todoList.childNodes;
	todos.forEach(function(todo){
		switch(e.target.value) {
			case "all":
				todo.style.display = "flex";
				break;
			case "completed":
				if(todo.classList.contains("completed")) {
					todo.style.display = "flex";
				}else{
					todo.style.display = "none";
				}
				break;
			case "uncompleted":
				if(!todo.classList.contains("completed")){
					todo.style.display = "flex";
				}else{
					todo.style.display = "none";
				}
				break;
		}
	});
}

function saveLocalTodos(todo) {
	//CHECK -- Do i have things in the local storage?
	let todos;
	if(localStorage.getItem("todos") === null) {
		todos = [];
	}else{
		//error encountere.... when parsing the keys in the local storage,dont state directly key's name. put it in a string like "todos"
		//not todo.
		todos = JSON.parse(localStorage.getItem("todos"));
	}

	todos.push(todo);
	localStorage.setItem("todos", JSON.stringify(todos));
}

function getTodos() {
	//CHECK -- Do i have things in the local storage?
	let todos;
	if(localStorage.getItem("todos") === null) {
		todos = [];
	}else{
		//error encountere.... when parsing the keys in the local storage,dont state directly key's name. put it in a string like "todos"
		//not todo.
		todos = JSON.parse(localStorage.getItem("todos"));
	}

	todos.forEach(function(todo){
		const todoDiv = document.createElement("div");
		todoDiv.classList.add("todo");

		const newTodo = document.createElement("li");
		newTodo.innerText = todo;
		newTodo.classList.add("todo-item");

		todoDiv.appendChild(newTodo);
		//Check Completed Butoon
		const completedButton = document.createElement("button");
		completedButton.innerHTML = '<i class="fas fa-check"></i>';
		completedButton.classList.add("complete-btn");
		todoDiv.appendChild(completedButton);
		//CHECK trash Button
		const trashButton = document.createElement("button");
		trashButton.innerHTML = '<i class="fas fa-trash"></i>';
		trashButton.classList.add("trash-btn");
		todoDiv.appendChild(trashButton);
		//APPEND TO LIST
		todoList.appendChild(todoDiv);		
	});
}

function removeLocalTodos(todo) {
	//CHECK -- Do i have things in the local storage?
	let todos;
	if(localStorage.getItem("todos") === null) {
		todos = [];
	}else{
		//error encountere.... when parsing the keys in the local storage,dont state directly key's name. put it in a string like "todos"
		//not todo.
		todos = JSON.parse(localStorage.getItem("todos"));
	}

	const todoIndex = todo.children[0].innerText;
	todos.splice(todos.indexOf(todoIndex), 1);
	localStorage.setItem("todos", JSON.stringify(todos));
}